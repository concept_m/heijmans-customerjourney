var gulp         = require('gulp'),
    sass         = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    config       = require('../config').sass;

gulp.task('sass', function() {
  return gulp.src(config.src)
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(autoprefixer("last 2 versions"))
    .pipe(gulp.dest(config.dest));
});
