var src  = './assets',
    dest = './build';

module.exports = {
  sass: {
    src: src + '/css/sass/**/*.scss',
    dest: src + '/css'
  }
};
