var actionsCarroussel;

$(document).ready(function(){


    if($('#action-list').length == 1){
        actionsCarroussel = new ActionsCarroussel();
        actionsCarroussel.init('action-list');
    }

    $('a.cat-link').on('click', function(){
        if(actionsCarroussel != undefined){
            $("html, body").animate({ scrollTop: 0 }, "slow");
            actionsCarroussel.chapter($(this).data('category'));
        }
        return false;
    });

    if($('#action-like').length == 1){
        var actionLikeHandler = new ActionLikeHandler();
        actionLikeHandler.init('action-like');
    }

    if($('.contacts-list').length == 1){
        $('.contacts-list .contact-name, .contacts-list .arrow').on('click', function(e){
            var li = $(e.currentTarget).parent();
            var ul = li.parent();
            var doOpen = true;

            if(li.hasClass('open')){
                doOpen = false;
            }

            ul.find('li').removeClass('open');
            if(doOpen){
                li.addClass('open');
            }

        });
    }

    $('.custom-select').on('click', function(e){
        var el = $(e.currentTarget);
        if(el.hasClass('open')){
            el.removeClass('open');
        }
        else{
            el.addClass('open');
        }
    });
    $('.custom-select li').on('click', function(e){
        var el = $(e.currentTarget);
        var container = el.parent().parent().parent();
        var title = container.find('.title .text');

        title.html(el.html());
        $('#' + container.data('related-input')).val(el.data('id'))
    });

    $('button.show-addform').on('click', function(e){
        $(e.currentTarget).blur();

        $('.add-form')  .css('opacity', 0)
                        .css('display', 'flex')
                        .animate({'opacity' : 1}, 'fast');
    });

    $('.close-addform').on('click', function(e){
        var el = $(e.currentTarget);
        el.blur();
        closeAddForm(el.parent().parent());

    });
});

function closeAddForm(el){
    $(el).fadeOut('fast');
}

function reloadPage(){
    closeAddForm('#add-form');
    location.reload();
}

function ActionLikeHandler(){

    var $this, radioYes, radioNo, form;

    this.init = function(el_id){
        $this = $('#' + el_id);
        form = $this.find('form');
        radioYes = $this.find('input.yes');
        radioNo = $this.find('input.no');

        $this.append('<button class="like like-yes"></button><button class="like like-no"></button>');

        $this.find('button.like').on('click', function(e){
            var btn = $(e.currentTarget);
            if(btn.hasClass('like-yes')){
                changeAndSubmitForm(true);
            }
            if(btn.hasClass('like-no')){
                changeAndSubmitForm(false);
            }
            btn.blur();
        });

        updateLikeButtons();
    }

    var changeAndSubmitForm = function(like){
        radioNo.removeAttr('checked');
        radioYes.removeAttr('checked');

        if(like === true){
            radioYes.prop('checked', true);
        }
        else{
            radioNo.prop('checked', true);
        }
        updateLikeButtons();
        form.submit();
    }

    var updateLikeButtons = function(){
        if(radioYes.is(':checked')){
            $this.find('button.like-yes').addClass('active');
            $this.find('button.like-no').removeClass('active');
        }

        else if(radioNo.is(':checked')){
            $this.find('button.like-yes').removeClass('active');
            $this.find('button.like-no').addClass('active');
        }
        else{
            $this.find('button.like-yes').addClass('active');
            $this.find('button.like-no').addClass('active');
        }
    }
}

function  ActionsCarroussel(){

    var $this, stage, circle, layer;

    var width = 600;
    var height = 600;
    var circle_radius = 230;
    var image_folder = '/themes/customerjourney/assets/images';
    var bgcolor = '#564F78';
    var activecolor = '#f1af2a';
    var degree = 0;
    var readyCounter = 0;

    var chapters = [
                    {   'name':'orienteren',
                        'icon':'icon_bulp.svg',
                        'icon_dimensions': [47,45],
                        'header':'header_orienteren.svg',
                        'header_position': [184,45,28],
                        'bg_color':'#564F78',
                        'header_color':'#FA5067'},
                    {   'name':'zoeken',
                        'icon':'icon_magn.svg',
                        'icon_dimensions': [49,36],
                        'header':'header_zoeken.svg',
                        'header_position': [180,60,26],
                        'bg_color':'#443E5E',
                        'header_color':'#E1324A'},
                    {   'name':'beslissen',
                        'icon':'icon_calc.svg',
                        'icon_dimensions': [51,49],
                        'header':'header_beslissen.svg',
                        'header_position': [180,55,30],
                        'bg_color':'#322D45',
                        'header_color':'#CA2C42'},
                    {   'name':'wachten',
                        'icon':'icon_clock.svg',
                        'icon_dimensions': [42,42],
                        'header':'header_wachten.svg',
                        'header_position': [180,54,28],
                        'bg_color':'#564F78',
                        'header_color':'#FA5067'},
                    {   'name':'verhuizen',
                        'icon':'icon_box.svg',
                        'icon_dimensions': [38,43],
                        'header':'header_verhuizen.svg',
                        'header_position': [184,45,28],
                        'bg_color':'#443E5E',
                        'header_color':'#E1324A'},
                    {   'name':'wonen',
                        'icon':'icon_home.svg',
                        'icon_dimensions': [45,42],
                        'header':'header_wonen.svg',
                        'header_position': [180,65,30],
                        'bg_color':'#322D45',
                        'header_color':'#CA2C42'}
                    ];

    this.chapter = function(chapter){
        showChapter(chapter);
    }


    this.init = function(el_id){
        $this = $('#' + el_id);
        $this.append('<div id="actionCarrousselCanvas"></div>');

        // stage = new createjs.Stage('actionCarrousselCanvas');

        stage = new Konva.Stage({
          container: 'actionCarrousselCanvas',
          width: width,
          height: height
        });


        layer = new Konva.Layer();

        circle = new Konva.Group({
            rotation: 130,
            width: circle_radius * 2,
            height: circle_radius * 2,
            x: stage.getWidth() / 2,
            y: stage.getHeight() / 2
        });

        var circle_bg = new Konva.Circle({
            x: stage.getWidth() / 2,
            y: stage.getHeight() / 2,
            radius: circle_radius,
            fill: bgcolor,
            opacity: 0
        });
        layer.add(circle_bg);

        var size = 360 / chapters.length;
        var startDeg = 0;
        var color = 0;
        $.each(chapters, function(index, value){

            var degree = startDeg;

            chapters[index]['degree'] = -degree-(size/2);

            var chapter = new Konva.Group({
                rotation: startDeg
            });

            var active = new Konva.Group({
                visible: false
            });

            chapters[index]['active'] = active;

            var arc = new Konva.Arc({
              innerRadius: 0,
              outerRadius: circle_radius - 80,
              fill: value['bg_color'],
              angle: size,
            });

            var white = new Konva.Arc({
              innerRadius: circle_radius - 80,
              outerRadius: circle_radius - 60,
              fill: '#ffffff',
              angle: size,
            });

            var grey = new Konva.Arc({
              innerRadius: circle_radius - 60,
              outerRadius: circle_radius - 55,
              fill: '#cccccc',
              angle: size,
            });

            var header = new Konva.Arc({
              innerRadius: circle_radius - 56,
              outerRadius: circle_radius,
              fill: value['header_color'],
              angle: size,
            });

            var iconObj = new Image();
            iconObj.onload = function(){
                var img = new Konva.Image({
                    x: 65,
                    y: 28,
                    width: value['icon_dimensions'][0],
                    height: value['icon_dimensions'][1],
                    image:iconObj,
                });
                chapter.add(img);

                readyCounter++;
                showIntro();
            }
            iconObj.src = image_folder + '/carroussel/' + value['icon'];

            var headerObj = new Image();
            headerObj.onload = function(){
                var img = new Konva.Image({
                    x: value['header_position'][0],
                    y: value['header_position'][1],
                    rotation: value['header_position'][2],
                    image:headerObj,
                });
                chapter.add(img);

                readyCounter++;
                showIntro();
            }
            headerObj.src = image_folder + '/carroussel/' + value['header'];

            var activeArc = new Konva.Arc({
              innerRadius: 0,
              outerRadius: circle_radius,
              fill: activecolor,
              angle: size,
            });

            active.add(activeArc);


            chapter.add(arc);
            chapter.add(grey);
            chapter.add(white);
            chapter.add(header);
            chapter.add(active);

            startDeg += size;

            chapter.on('mouseup', function() {
              showChapter(value['name']);
            });



            circle.add(chapter);
        });

        circle.on('mouseover', function() {
            document.body.style.cursor = 'pointer';
        });
        circle.on('mouseout', function() {
            document.body.style.cursor = 'default';
        });

        layer.add(circle);

        stage.add(layer);
    }

    var showIntro = function(){
        if(readyCounter == 12){

            $this.fadeIn();

            setTimeout(function() {
                showChapter(chapters[0]['name']);
            }, 1500);
        }
    }

    var rotateTo = function(end_degree){
        if(end_degree != degree){
            degree = end_degree;
            new Konva.Tween({
                node: circle,
                duration: 1.2,
                rotation: degree,
                easing: Konva.Easings.ElasticEaseInOut
            }).play();
        }
    }

    var showChapter = function(chapter){

        $.each(chapters, function(index, value){
            if(value.name == chapter){
                value.active.visible(true);
                rotateTo(value.degree);
            }
            else{
                value.active.visible(false);
            }
        });

        setTimeout(function() {
            $('.cat-wrapper').hide();
            $('#cat-'+chapter).fadeIn('slow');
        }, 500);
    }

}

function onAddRelation(){
    alert('go!!!');
}
