<?php namespace Conceptm\Customerjourney;

use Backend;

class Plugin extends \System\Classes\PluginBase
{
    public $require = ['RainLab\User']; // for future like-functionality

    public function pluginDetails()
    {
        return [
            'name' => 'Customerjourney',
            'description' => 'Plugin for Heijmans Customer Journey website',
            'author' => 'Concept M',
            'icon' => 'icon-rocket'
        ];
    }

    public function registerComponents()
    {
        return [
            'Conceptm\Customerjourney\Components\Actionlist' => 'actionList',
            'Conceptm\Customerjourney\Components\Actiondetail' => 'actionDetail',
            'Conceptm\Customerjourney\Components\Actiontoplist' => 'actionTopList',
            'Conceptm\Customerjourney\Components\Addrelation' => 'addRelation',
            'Conceptm\Customerjourney\Components\Flatlist' => 'Flatlist',
            'Conceptm\Customerjourney\Components\ProjectDetail' => 'projectDetail',
            'Conceptm\Customerjourney\Components\ProjectList' => 'projectList',
            'Conceptm\Customerjourney\Components\RandomAction' => 'randomAction',
            'Conceptm\Customerjourney\Components\SkbList' => 'skbList',
        ];
    }

    public function registerReportWidgets()
    {
        return [
            'Conceptm\Customerjourney\ReportWidgets\UserStats' => [
                'label'   => 'User Stats',
                'context' => 'dashboard'
            ]
        ];
    }

    public function registerNavigation()
    {
        return [
            'customerjourney' => [
                'label'       => 'Customer Journey',
                'url'         => Backend::url('conceptm/customerjourney/actions'),
                'icon'        => 'icon-rocket',
                'permissions' => ['rainlab.users.*'],
                'order'       => 500,

                'sideMenu' => [
                    'actions' => [
                        'label'       => 'Acties',
                        'icon'        => 'icon-rocket',
                        'url'         => Backend::url('conceptm/customerjourney/actions'),
                    ],
                    'projects' => [
                        'label'       => 'Projecten',
                        'icon'        => 'icon-briefcase',
                        'url'         => Backend::url('conceptm/customerjourney/projects'),
                    ],
                    'contacts' => [
                        'label'       => 'Contactpersonen',
                        'icon'        => 'icon-users',
                        'url'         => Backend::url('conceptm/customerjourney/contacts'),
                    ],
                    'skbs' => [
                        'label'       => 'SKB\'s',
                        'icon'        => 'icon-line-chart',
                        'url'         => Backend::url('conceptm/customerjourney/skbs'),
                    ]
                ]
            ]
        ];
    }
}
