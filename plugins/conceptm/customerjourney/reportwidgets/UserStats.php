<?php

namespace Conceptm\Customerjourney\ReportWidgets;

use Backend\Classes\ReportWidgetBase;
use RainLab\User\Models\User as UserModel;

class UserStats extends ReportWidgetBase
{
    public $data;

    public function render()
    {
        $this->data = (new UserModel)->where('is_activated', 0)->orderBy('created_at', 'desc')->get();

        return $this->makePartial('widget');
    }
}
