<?php namespace Conceptm\Customerjourney\Controllers;

use BackendMenu;

class Actions extends \Backend\Classes\Controller {

    public $implement = [   'Backend.Behaviors.ListController',
                            'Backend.Behaviors.FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Conceptm.Customerjourney', 'customerjourney', 'actions');
    }

    public function index()
    {
        $this->bodyClass = 'slim-container';
        $this->makeLists();
    }
}
