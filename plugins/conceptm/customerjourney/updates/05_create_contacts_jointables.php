<?php namespace Conceptm\Customerjourney\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateContactsJoinTable extends Migration
{

    public function up()
    {

        Schema::create('conceptm_customerjourney_actions_contacts', function($table)
        {
            $table->integer('action_id')->unsigned();
            $table->integer('contact_id')->unsigned();
            $table->primary(['action_id', 'contact_id'], 'conceptm_customerjourney_actions_contacts_primary');
        });

        Schema::create('conceptm_customerjourney_contacts_projects', function($table)
        {
            $table->integer('contact_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->primary(['contact_id', 'project_id'], 'conceptm_customerjourney_contacts_projects_primary');
        });
    }

    public function down()
    {
        Schema::dropIfExists('conceptm_customerjourney_actions_contacts');
        Schema::dropIfExists('conceptm_customerjourney_contacts_projects');
    }

}
