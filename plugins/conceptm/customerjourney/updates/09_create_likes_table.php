<?php namespace Conceptm\Customerjourney\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateLikesTable extends Migration
{

    public function up()
    {
        Schema::create('conceptm_customerjourney_likes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('action_id');
            $table->integer('user_id');
            $table->boolean('like');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('conceptm_customerjourney_likes');
    }

}
