<?php namespace Conceptm\Customerjourney\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateActionsProjectsJoinTable extends Migration
{

    public function up()
    {

        Schema::create('conceptm_customerjourney_actions_projects', function($table)
        {
            $table->integer('action_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->primary(['action_id', 'project_id'], 'conceptm_customerjourney_actions_projects_primary');
        });
    }

    public function down()
    {
        Schema::dropIfExists('conceptm_customerjourney_actions_projects');
    }

}
