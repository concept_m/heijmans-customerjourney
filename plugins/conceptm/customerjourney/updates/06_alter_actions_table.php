<?php namespace Conceptm\Customerjourney\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterActionsTable extends Migration
{

    public function up()
    {
        Schema::table('conceptm_customerjourney_actions', function($table)
        {
            $table->string('category')->after('title');
            $table->string('number')->after('title');
        });
    }
}
