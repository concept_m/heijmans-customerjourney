<?php namespace Conceptm\Customerjourney\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterActionsTable extends Migration
{

    public function up()
    {
        Schema::table('conceptm_customerjourney_actions', function($table)
        {
            $table->string('full_title')->after('links');
        });

        Schema::table('conceptm_customerjourney_contacts', function($table)
        {
            $table->string('full_name')->after('phone');
        });
    }
}
