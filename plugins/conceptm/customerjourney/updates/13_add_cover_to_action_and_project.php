<?php namespace Conceptm\Customerjourney\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddCoverToActionProject extends Migration
{

    public function up()
    {
        Schema::table('conceptm_customerjourney_actions', function($table)
        {
            $table->string('cover')->after('category');
        });

        Schema::table('conceptm_customerjourney_projects', function($table)
        {
            $table->string('cover')->after('url');
        });
    }
}
