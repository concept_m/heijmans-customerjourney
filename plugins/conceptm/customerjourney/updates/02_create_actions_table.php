<?php namespace Conceptm\Customerjourney\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateActionsTable extends Migration
{

    public function up()
    {
        Schema::create('conceptm_customerjourney_actions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->longText('intro');
            $table->longText('why');
            $table->longText('what');
            $table->longText('how');
            $table->boolean('is_published')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('conceptm_customerjourney_actions');
    }

}
