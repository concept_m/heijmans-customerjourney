<?php namespace Conceptm\Customerjourney\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddActionAmbassador extends Migration
{

    public function up()
    {
        Schema::table('conceptm_customerjourney_actions', function($table)
        {
            $table->integer('ambassador_id')->after('category');
        });
    }
}
