<?php namespace Conceptm\Customerjourney\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateSkbsTable extends Migration
{

    public function up()
    {
        Schema::create('conceptm_customerjourney_skbs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('rate');
            $table->string('region');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('conceptm_customerjourney_skbs');
    }

}
