<?php namespace Conceptm\Customerjourney\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterActionsTable extends Migration
{

    public function up()
    {
        Schema::table('conceptm_customerjourney_actions', function($table)
        {
            $table->string('slug')->after('title');
        });

        Schema::table('conceptm_customerjourney_projects', function($table)
        {
            $table->string('slug')->after('name');
        });
    }
}
