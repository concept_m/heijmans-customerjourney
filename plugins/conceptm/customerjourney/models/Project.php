<?php namespace Conceptm\Customerjourney\Models;

use Model;
use System\Models\File;

class Project extends Model{
    protected $table = 'conceptm_customerjourney_projects';

    public $attachOne = [
        'image' => ['System\Models\File']
    ];

    public $belongsToMany = [
        'actions' => ['Conceptm\Customerjourney\Models\Action', 'table' => 'conceptm_customerjourney_actions_projects', 'conditions' => 'is_published = 1', 'order' => 'number'],
        'contacts' => ['Conceptm\Customerjourney\Models\Contact', 'table' => 'conceptm_customerjourney_contacts_projects', 'order' => 'surname']
    ];
}
