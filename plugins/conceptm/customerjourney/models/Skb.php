<?php namespace Conceptm\Customerjourney\Models;

use Model;

/**
 * Skb Model
 */
class Skb extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'conceptm_customerjourney_skbs';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}