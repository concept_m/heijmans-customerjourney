<?php namespace Conceptm\Customerjourney\Models;

use Model;
use System\Models\File;

class Like extends Model{
    protected $table = 'conceptm_customerjourney_likes';

    public $belongsTo = [
        'actions' => ['Conceptm\Customerjourney\Models\Action', 'table' => 'conceptm_customerjourney_actions_projects'],
        'users' => ['RainLab\User\Models\User', 'table' => 'users']
    ];

    public function saveLike($user_id, $action_id, $like){
        $data = $this  ->where('user_id', $user_id)
                        ->where('action_id', $action_id)
                        ->get();

        if(sizeof($data) == 0){
            $new = new Like;
            $new->user_id = $user_id;
            $new->action_id = $action_id;
            if($like === 'no'){
                $new->like = 0;
            }
            else{
                $new->like = 1;
            }
            $new->save();
        }
        elseif(sizeof($data) == 1){
            $item = $data[0];
            if($like === 'no'){
                $item->like = 0;
            }
            else{
                $item->like = 1;
            }
            $item->save();
        }
    }
}
