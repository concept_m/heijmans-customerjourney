<?php namespace Conceptm\Customerjourney\Models;

use Model;
use DB;
use System\Models\File;

class Action extends Model{
    protected $table = 'conceptm_customerjourney_actions';
    protected $jsonable = ['links'];

    protected $categories = [    'orienteren' => ['title' => 'Oriënteren', 'subtitle' => 'Dit is de fase waarin onze klanten niet bewust aan het zoeken zijn. Onze klant is lekker aan het dromen en wij zijn volop in verkoop. Wij moeten vooral zichtbaar zijn, en eruit springen ten opzichte van de rest.'],
                                'zoeken' => ['title' => 'Zoeken', 'subtitle' => 'De klant weet inmiddels wat hij wil. De plaats, omgeving en financiën zijn op hoofdlijnen inzichtelijk. Voor ons nog steeds de verkoopfase waarin wij ons willen onderscheiden.'],
                                'beslissen' => ['title' => 'Beslissen', 'subtitle' => 'Tijd om te tekenen, leg je koper in de watten en begeleid hem naar (veelal) de grootste aankoop in zijn leven.'],
                                'wachten' => ['title' => 'Wachten', 'subtitle' => 'Voor ons de tijd van bouwvoorbereiding en realisatie, voor de klant een hectische tijd over meer en minder werk en vooral wachten tot zijn woning klaar is. Informeer ze goed!'],
                                'verhuizen' => ['title' => 'Verhuizen', 'subtitle' => 'De woning is klaar. Het moment van feestelijk opleveren is een feit. Zet je klant in het zonnetje!'],
                                'wonen' => ['title' => 'Wonen', 'subtitle' => 'Houten vloer, gietvloer, welke klei op de muur. Onze kopers zijn druk met klussen maar onze garantietermijn zit er bijna op, stuur een kaart en informeer ze, zo blijven ze tevreden!'] ];

    public $attachOne = [
        'image' => ['System\Models\File'],
        'memo' => ['System\Models\File']
    ];

    public $attachMany = [
        'files' => ['System\Models\File']
    ];

    public $belongsToMany = [
        'projects' => ['Conceptm\Customerjourney\Models\Project', 'table' => 'conceptm_customerjourney_actions_projects', 'conditions' => 'is_published = 1'],
        'contacts' => ['Conceptm\Customerjourney\Models\Contact', 'table' => 'conceptm_customerjourney_actions_contacts', 'order' => 'surname']
    ];

    public $hasMany = [
        'likes' => ['Conceptm\Customerjourney\Models\Like', 'table' => 'conceptm_customerjourney_actions_likes']
    ];

    public $belongsTo = [
        'ambassador' => ['Conceptm\Customerjourney\Models\Contact', 'table' => 'conceptm_customerjourney_actions_contacts']
    ];

    public function beforeSave()
    {
        // Generate a URL slug for this model
        $this->full_title = $this->number.' - '.$this->title;
    }


    public function getCategoryOptions()
    {

        $return = [];
        foreach($this->categories as $key => $item){
            $return[$key] = $item['title'];
        }

        return $return;
    }

    public function getCategoriesBySlugs($slugs){
        $return = null;
        foreach($slugs as $slug){
            $return[] = [ 'slug' => $slug,
                            'title' => $this->categories[$slug]['title']];
        }
        return $return;
    }

    public function getCategorizedList($category = null)
    {
        $return = array();

        // one category or all?
        if(!is_null($category) && isset($this->categories[$category])){
            // fill return with category
            $return[$category] = [  'title' => $this->categories[$category]['title'],
                                    'subtitle' => $this->categories[$category]['subtitle'],
                                    'slug' => $category,
                                    'actions' => array()];

            // get model data
            $data = $this->selectRaw($this->table.'.*, count(conceptm_customerjourney_likes.action_id) as aggregate')
                        ->leftJoin('conceptm_customerjourney_likes', 'conceptm_customerjourney_likes.action_id', '=', $this->table.'.id')
                        ->where($this->table.'.category', $category)
                        ->where($this->table.'.is_published', 1)
                        ->groupBy($this->table.'.id')
                        ->orderBy($this->table.'.number','ASC')
                        ->get();
        }
        else{
            // fill return with all categories
            foreach($this->categories as $key=>$item){
                $return[$key] = [   'title' => $item['title'],
                                    'subtitle' => $item['subtitle'],
                                    'slug' => $key,
                                    'actions' => array()];
            }

            // get model data
            $data = $this->selectRaw($this->table.'.*, count(conceptm_customerjourney_likes.action_id) as aggregate')
                        ->leftJoin('conceptm_customerjourney_likes', 'conceptm_customerjourney_likes.action_id', '=', $this->table.'.id')
                        ->where($this->table.'.is_published', 1)
                        ->groupBy($this->table.'.id')
                        ->orderBy($this->table.'.number','ASC')
                        ->get();
        }

        // fill return with model data
        foreach($data as $item){
            $return[$item->category]['actions'][] = $item;
        }

        // return it all
        return $return;
    }

    public function getPopularList($limit)
    {
        $data = $this    ->selectRaw($this->table.'.*, count('.$this->table.'.id) as aggregate')
                        ->join('conceptm_customerjourney_likes', 'conceptm_customerjourney_likes.action_id', '=', $this->table.'.id')
                        ->where('conceptm_customerjourney_likes.like', 1)
                        ->where($this->table.'.is_published', 1)
                        ->groupBy('conceptm_customerjourney_likes.action_id')
                        ->orderBy('aggregate', 'DESC')
                        ->take($limit)
                        ->get();

        foreach($data as &$item){
            $item['category_title'] = $this->categories[$item['category']]['title'];
        }

        return $data;
    }

    public function getItem($category, $slug){
        $item = $this   ->where('is_published', 1)
                        ->where('category', $category)
                        ->where('slug', $slug)
                        ->get();



        if(sizeof($item) == 0){
            return null;
        }
        else{
            $return = $item[0];

            $neighbours = $this->getNeighbours($item[0]['id']);

            if(!is_null($neighbours)){
                $return['neighbours'] = $neighbours;
            }

            return $return;
        }
    }

    public function getNeighbours($id){
        $return = null;

        $list = $this   ->where('is_published', 1)
                        ->orderBy('number')
                        ->get();
        $pos = null;
        foreach($list as $key=>$item){
            if($item['id'] == $id)
            $pos = $key;
        }
        if(!is_null($pos)){
            if($pos > 0){
                $return['prev'] = $list[$pos-1];
            }
            if($pos < sizeof($list) - 1){
                $return['next'] = $list[$pos+1];
            }
        }
        return $return;
    }

    public function getRandomItem(){
        return $this->orderBy(DB::raw('RAND()'))->where($this->table.'.is_published', 1)->first();
    }
}
