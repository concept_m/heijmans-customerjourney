<?php namespace Conceptm\Customerjourney\Models;

use Model;
use System\Models\File;

class Contact extends Model{
    protected $table = 'conceptm_customerjourney_contacts';

    public $belongsToMany = [
        'projects' => ['Conceptm\Customerjourney\Models\Project', 'table' => 'conceptm_customerjourney_contacts_projects'],
        'actions' => ['Conceptm\Customerjourney\Models\Action', 'table' => 'conceptm_customerjourney_actions_contacts']
    ];

    public function beforeSave()
    {
        // Generate a URL slug for this model
        $this->full_name = $this->name.' '.$this->surname;
    }
}
