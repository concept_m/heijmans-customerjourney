<?php namespace Conceptm\Customerjourney\Components;

use Cms\Classes\ComponentBase;
use Conceptm\Customerjourney\Models\Action as ActionModel;
use Conceptm\Customerjourney\Models\Project as ProjectModel;


class Flatlist extends ComponentBase
{

    public $type;
    public $data;

    public function componentDetails()
    {
        return [
            'name'        => 'Flatlist',
            'description' => 'Creates a simple list of actions of projects'
        ];
    }

    public function defineProperties()
    {
        return [

            'type' => [
                'title'       => 'Type',
                'type'        => 'dropdown',
                'placeholder' => 'Selecteer een type',
            ]

        ];
    }

    public function getTypeOptions()
    {
        return ['actions' => 'Actie', 'projects' => 'Project'];
    }

    public function onRun(){
        $this->type = $this->property('type');
        $options = $this->getTypeOptions();

        if(!isset($options[$this->type])){
            $this->type = 'actions';
        }

        if($this->type == 'projects'){
            $this->data = (new ProjectModel)    ->where('is_published', 1)
                                                ->orderBy('name','ASC')
                                                ->get();
        }
        elseif($this->type == 'actions'){
            $this->data = (new ActionModel)    ->where('is_published', 1)
                                                ->orderBy('number','ASC')
                                                ->get();
        }
    }

}
