<?php namespace Conceptm\Customerjourney\Components;

use Cms\Classes\ComponentBase;
use Conceptm\Customerjourney\Models\Action as ActionModel;

class RandomAction extends ComponentBase
{
    public $item;

    public function componentDetails()
    {
        return [
            'name'        => 'RandomAction',
            'description' => 'Show one random action'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $this->item = (new ActionModel)->getRandomItem();
    }

}
