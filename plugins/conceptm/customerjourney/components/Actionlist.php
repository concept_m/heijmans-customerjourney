<?php namespace Conceptm\Customerjourney\Components;

use Cms\Classes\ComponentBase;
use Conceptm\Customerjourney\Models\Action as ActionModel;

class Actionlist extends ComponentBase
{

    public $list;
    public $list_id = 'action-list';

    public function componentDetails()
    {
        return [
            'name'        => 'Actionlist',
            'description' => 'Provides a categorized collection of Actions'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){

        if(!is_null(gettype($this->property('list_id')))){
            $this->list_id = $this->property('list_id');
        }

        $this->list = (new ActionModel)->getCategorizedList();
    }

}
