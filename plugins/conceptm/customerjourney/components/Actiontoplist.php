<?php namespace Conceptm\Customerjourney\Components;

use Cms\Classes\ComponentBase;
use Conceptm\Customerjourney\Models\Action as ActionModel;

class Actiontoplist extends ComponentBase
{

    public $list;

    public function componentDetails()
    {
        return [
            'name'        => 'Actiontoplist',
            'description' => 'Gives the top 5 popular actions'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){

        $this->list = (new ActionModel)->getPopularList(5);
    }

}
