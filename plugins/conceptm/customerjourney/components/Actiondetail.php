<?php namespace Conceptm\Customerjourney\Components;

use Auth;
use Cms\Classes\ComponentBase;
use Conceptm\Customerjourney\Models\Action as ActionModel;
use Conceptm\Customerjourney\Models\Like as LikeModel;

class Actiondetail extends ComponentBase
{
    public $data;
    public $user;
    public $category;

    public function componentDetails()
    {
        return [
            'name'        => 'Actiondetail',
            'description' => 'Shows all data of an Action'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $category = $this->property('category');
        $action = $this->property('action');

        $this->data = (new ActionModel)->getItem($category, $action);

        $this->category = (new ActionModel)->categories[$category];

        $this->user = Auth::getUser();

        $this->page->title = $this->data['title'];
    }

    public function onSaveLike(){
        $user = Auth::getUser();
        $action_id = post('action');
        $like = post('like');

        (new LikeModel)->saveLike($user->id, $action_id, $like);
    }
}
