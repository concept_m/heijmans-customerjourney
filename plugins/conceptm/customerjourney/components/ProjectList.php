<?php namespace Conceptm\Customerjourney\Components;

use Cms\Classes\ComponentBase;
use Conceptm\Customerjourney\Models\Project as ProjectModel;

class ProjectList extends ComponentBase
{

    public $list;

    public function componentDetails()
    {
        return [
            'name'        => 'ProjectList',
            'description' => 'Provides a list of all projects'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){

        $this->list = (new ProjectModel)->where('is_published', 1)
                                        ->orderBy('name', 'asc')
                                        ->get();
    }

}
