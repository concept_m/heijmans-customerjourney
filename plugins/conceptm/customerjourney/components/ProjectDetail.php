<?php namespace Conceptm\Customerjourney\Components;

use Cms\Classes\ComponentBase;
use Conceptm\Customerjourney\Models\Project as ProjectModel;
use Conceptm\Customerjourney\Models\Action as ActionModel;

class ProjectDetail extends ComponentBase
{

    public $data;
    public $categories;

    public function componentDetails()
    {
        return [
            'name'        => 'ProjectDetail',
            'description' => 'Shows all data of a Project'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $project = $this->property('project');



        $this->data = (new ProjectModel)   ->where('slug', $project)
                                    ->where('is_published', 1)
                                    ->first();

        $cats = [];
        foreach($this->data['actions'] as $action){
            $cats[$action['category']] = true;
        }
        $this->categories = (new ActionModel)->getCategoriesBySlugs(array_keys($cats));


        $this->page->title = $this->data['name'];
    }

}
