<?php namespace Conceptm\Customerjourney\Components;

use Cms\Classes\ComponentBase;
use Conceptm\Customerjourney\Models\Action as ActionModel;
use Conceptm\Customerjourney\Models\Project as ProjectModel;

class Addrelation extends ComponentBase
{

    public $type;
    public $data;
    public $item_id;

    public function componentDetails()
    {
        return [
            'name'        => 'Addrelation',
            'description' => 'Add a relation between projects and actions'
        ];
    }

    public function defineProperties()
    {
        return [

            'type' => [
                'title'       => 'Type',
                'type'        => 'dropdown',
                'placeholder' => 'Selecteer een type',
            ]

        ];
    }

    public function getTypeOptions()
    {
        return ['actions' => 'Actie', 'projects' => 'Project'];
    }

    public function onRun()
    {
        $this->type = $this->property('type');
        $options = $this->getTypeOptions();

        if(!isset($options[$this->type])){
            $this->type = 'actions';
        }

        if($this->type == 'actions'){

            $project = (new ProjectModel)        ->where('is_published', 1)
                                                ->where('slug', $this->property('project'))
                                                ->first();
            if(isset($project['id'])){
                $this->item_id = $project['id'];
            }

            $this->data = (new ActionModel)    ->where('is_published', 1)
                                                ->orderBy('number','ASC')
                                                ->get();
        }
        elseif($this->type == 'projects'){
            $action = (new ActionModel)    ->where('is_published', 1)
                                                ->where('slug', $this->property('action'))
                                                ->first();
            if(isset($action['id'])){
                $this->item_id = $action['id'];
            }

            $this->data = (new ProjectModel)    ->where('is_published', 1)
                                                ->orderBy('name','ASC')
                                                ->get();
        }

    }

    public function onAddRelation()
    {
        $action_id = post('action');
        $project_id = post('project');

        if(!empty($action_id) && !empty($project_id)){
            $project = (new ProjectModel)->find($project_id);
            if($project->actions()->find($action_id) == null){
                $project->actions()->attach($action_id);
            }
        }
    }

}
