<?php namespace Conceptm\Customerjourney\Components;

use Cms\Classes\ComponentBase;
use Conceptm\Customerjourney\Models\Skb as SkbModel;

class SkbList extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'SkbList',
            'description' => 'Provides a list of all SKB Regions'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){

        $this->list = (new SkbModel)->orderBy('region','ASC')->get();
    }

}
