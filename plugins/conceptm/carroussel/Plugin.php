<?php namespace Conceptm\Carroussel;

use System\Classes\PluginBase;
use Backend;

/**
 * Carroussel Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Carroussel',
            'description' => 'Image carroussel with texts',
            'author'      => 'Concept M',
            'icon'        => 'icon-flask'
        ];
    }

    public function registerComponents()
    {
        return [
            'Conceptm\Carroussel\Components\Slick' => 'slick'
        ];
    }

    public function registerNavigation()
    {
        return [
            'carroussels' => [
                'label'       => 'Carroussels',
                'url'         => Backend::url('conceptm/carroussel/carroussels'),
                'icon'        => 'icon-flask',
                'permissions' => ['rainlab.users.*'],
                'order'       => 500,

                'sideMenu' => [
                    'actions' => [
                        'label'       => 'Carroussels',
                        'icon'        => 'icon-flask',
                        'url'         => Backend::url('conceptm/carroussel/carroussels'),
                    ]
                ]
            ]
        ];
    }

}
