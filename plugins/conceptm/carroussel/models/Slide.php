<?php namespace Conceptm\Carroussel\Models;

use Model;

/**
 * Slide Model
 */
class Slide extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'conceptm_carroussel_slides';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['quote','by','link','number'];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'carroussels' => ['Conceptm\Carroussel\Models\Carroussel', 'table' => 'conceptm_carroussel_carroussels']
    ];
    public $attachOne = [
        'image' => ['System\Models\File'],
    ];

}
