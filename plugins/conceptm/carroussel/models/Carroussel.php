<?php namespace Conceptm\Carroussel\Models;

use Model;

/**
 * Carroussel Model
 */
class Carroussel extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'conceptm_carroussel_carroussels';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'slides' => ['Conceptm\Carroussel\Models\Slide', 'table' => 'conceptm_carroussel_slides', 'order' => 'number']
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}
