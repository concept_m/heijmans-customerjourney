<?php namespace Conceptm\Carroussel\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateSlidesTable extends Migration
{

    public function up()
    {
        Schema::create('conceptm_carroussel_slides', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('carroussel_id');
            $table->longText('quote');
            $table->string('by');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('conceptm_carroussel_slides');
    }

}
