<?php namespace Conceptm\Carroussel\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateCarrousselsTable extends Migration
{

    public function up()
    {
        Schema::create('conceptm_carroussel_carroussels', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('conceptm_carroussel_carroussels');
    }

}
