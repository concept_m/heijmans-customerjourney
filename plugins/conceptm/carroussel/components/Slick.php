<?php namespace Conceptm\Carroussel\Components;

use Cms\Classes\ComponentBase;
use Conceptm\Carroussel\Models\Carroussel as Carroussel;

class Slick extends ComponentBase
{

    public $data;

    public function componentDetails()
    {
        return [
            'name'        => 'Slick',
            'description' => 'Carroussel component, based on slick-carousel'
        ];
    }

    public function defineProperties()
    {
        return [

            'item' => [
                'title'       => 'Item',
                'type'        => 'dropdown',
                'placeholder' => 'Selecteer een item'
            ]

        ];
    }

    public function getItemOptions()
    {
        return (new Carroussel)->lists('title', 'id');
    }

    public function onRun(){
        $item = $this->property('item');

        $this->data = (new Carroussel)->where('id', $item)->first();

        $this->addJs('/plugins/conceptm/carroussel/assets/js/slick.min.js');
        $this->addJs('/plugins/conceptm/carroussel/assets/js/init.js');
        $this->addCss('/plugins/conceptm/carroussel/assets/css/slick.css');
        $this->addCss('/plugins/conceptm/carroussel/assets/css/slick-theme.css');
    }

}
